#ifndef MENUCOORDENADOR_H
#define MENUCOORDENADOR_H

#include <QDialog>

namespace Ui {
class MenuCoordenador;
}

class MenuCoordenador : public QDialog
{
    Q_OBJECT

public:
    explicit MenuCoordenador(QWidget *parent = 0);
    ~MenuCoordenador();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MenuCoordenador *ui;
};

#endif // MENUCOORDENADOR_H
