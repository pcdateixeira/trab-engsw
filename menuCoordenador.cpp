#include "menuCoordenador.h"
#include "cadastroFuncionario.h"
#include "cadastroAluno.h"
#include "ui_menuCoordenador.h"
#include "usuario.h"

MenuCoordenador::MenuCoordenador(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MenuCoordenador)
{
    ui->setupUi(this);
}

MenuCoordenador::~MenuCoordenador()
{
    delete ui;
}

void MenuCoordenador::on_pushButton_clicked()
{
    CadastroFuncionario cadastro;
    cadastro.exec();
}

void MenuCoordenador::on_pushButton_2_clicked()
{
    CadastroAluno cadastro;
    cadastro.exec();
}

void MenuCoordenador::on_pushButton_3_clicked() // logout
{
    hide();

    Usuario user;

    user.efetuarLogin();
}
