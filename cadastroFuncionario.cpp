#include "cadastroFuncionario.h"
#include "ui_cadastroFuncionario.h"

CadastroFuncionario::CadastroFuncionario(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CadastroFuncionario)
{
    ui->setupUi(this);
}

CadastroFuncionario::~CadastroFuncionario()
{
    delete ui;
}

void CadastroFuncionario::on_buttonBox_accepted()
{
    close();
}

void CadastroFuncionario::on_buttonBox_rejected()
{
    close();
}
