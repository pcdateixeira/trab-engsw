#include "cadastroAluno.h"
#include "ui_cadastroAluno.h"

CadastroAluno::CadastroAluno(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CadastroAluno)
{
    ui->setupUi(this);
}

CadastroAluno::~CadastroAluno()
{
    delete ui;
}

void CadastroAluno::on_buttonBox_accepted()
{
    close();
}

void CadastroAluno::on_buttonBox_rejected()
{
    close();
}
