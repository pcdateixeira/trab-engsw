#ifndef FUNCIONARIO_H
#define FUNCIONARIO_H

#include "usuario.h"

class Funcionario:Usuario
{

    private:
        int id;
        std::string nome;
        std::string cargo;
        std::string unidade;
        std::string email;


    public:
        Funcionario(std::string nome, std::string cargo, std::string unidade, std::string email);
};

#endif // FUNCIONARIO_H
