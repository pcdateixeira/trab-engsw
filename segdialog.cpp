#include "segdialog.h"
#include "ui_segdialog.h"

SegDialog::SegDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SegDialog)
{
    ui->setupUi(this);
}

SegDialog::~SegDialog()
{
    delete ui;
}
