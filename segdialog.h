#ifndef SEGDIALOG_H
#define SEGDIALOG_H

#include <QDialog>

namespace Ui {
class SegDialog;
}

class SegDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SegDialog(QWidget *parent = nullptr);
    ~SegDialog();

private:
    Ui::SegDialog *ui;
};

#endif // SEGDIALOG_H
