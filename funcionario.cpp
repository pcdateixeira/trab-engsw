#include "funcionario.h"

Funcionario::Funcionario(std::string nome, std::string cargo, std::string unidade, std::string email):Usuario()
{
    this->nome = nome;
    this->cargo = cargo;
    this->unidade = unidade;
    this->email = email;
}
