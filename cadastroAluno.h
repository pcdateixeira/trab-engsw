#ifndef CADASTROALUNO_H
#define CADASTROALUNO_H

#include <QDialog>

namespace Ui {
class CadastroAluno;
}

class CadastroAluno : public QDialog
{
    Q_OBJECT

public:
    explicit CadastroAluno(QWidget *parent = 0);
    ~CadastroAluno();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::CadastroAluno *ui;
};

#endif // CADASTROALUNO_H
