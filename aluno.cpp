#include "aluno.h"

Aluno::Aluno(std::string nome, std::string unidade, std::string serie, std::string turma, std::string responsavel, std::string email):Usuario()
{
    this->nome = nome;
    this->unidade = unidade;
    this->serie = serie;
    this->turma = turma;
    this->responsavel = responsavel;
    this->email = email;
}
