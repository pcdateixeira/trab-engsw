#include "mainwindow.h"
#include "menuCoordenador.h"
#include "ui_mainwindow.h"
#include "usuario.h"
#include <QMessageBox>

//extern map usuarios;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    QString username = "Joninhas";
    QString password = "1234";

    logins.insert(std::make_pair(std::make_pair(username, password), 1));
    usuarios.insert(std::make_pair(1, 1));

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    QString username = ui->lineEdit_username->text();
    QString password = ui->lineEdit_password->text();

    //if(username == "Joninhas" && password == "1234"){
    if(logins.find({username, password}) != logins.end())
    {
        hide();

        MenuCoordenador coordenador;
        coordenador.exec();

    }else{
        QMessageBox::warning(this, "Login", "Incorrect username or password");

    }
}

void MainWindow::on_lineEdit_password_returnPressed()
{

    on_pushButton_clicked();

}

void MainWindow::on_lineEdit_username_returnPressed()
{
    on_pushButton_clicked();
}


