#ifndef ALUNO_H
#define ALUNO_H

#include "usuario.h"

class Aluno:Usuario
{

    private:
        int id;
        std::string nome;
        std::string unidade;
        std::string serie;
        std::string turma;
        std::string responsavel;
        std::string email;

    public:
        Aluno(std::string nome, std::string unidade, std::string serie, std::string turma, std::string responsavel, std::string email);
};

#endif // ALUNO_H
