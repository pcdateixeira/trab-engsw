#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "segdialog.h"

//extern std::map<std::pair<QString, QString>, std::pair<int, int>> usuarios;
//usuarios.insert(std::make_pair(std::make_pair(username, password), std::make_pair(1, 1)));


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void on_pushButton_clicked();

    void on_lineEdit_password_returnPressed();

    void on_lineEdit_username_returnPressed();



private:
    Ui::MainWindow *ui;
    SegDialog *segDialog;
    std::map<std::pair<QString, QString>, int> logins;
    std::map<int, int> usuarios;
};
#endif // MAINWINDOW_H
