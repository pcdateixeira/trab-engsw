#ifndef CADASTROFUNCIONARIO_H
#define CADASTROFUNCIONARIO_H

#include <QDialog>

namespace Ui {
class CadastroFuncionario;
}

class CadastroFuncionario : public QDialog
{
    Q_OBJECT

public:
    explicit CadastroFuncionario(QWidget *parent = 0);
    ~CadastroFuncionario();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::CadastroFuncionario *ui;
};

#endif // CADASTROFUNCIONARIO_H
